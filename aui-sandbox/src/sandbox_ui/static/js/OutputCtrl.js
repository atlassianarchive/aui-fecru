define("OutputCtrl", ["MainModule"], function(sandboxModule) {
    sandboxModule.controller("OutputCtrl", OutputCtrl);

    function OutputCtrl($scope) {
        var iframeWindow, iframeDocument;
        var $head, $body, $css, $js;

        AJS.$('body').bind('runJavascript', function() {
            $scope.outputAndRunJs();
        });

        $scope.bindToEditorChange(null, function() {
            $scope.output();
        });

        function setupIframe(){
            // set up the guts of the iframe
            iframeWindow = AJS.$('#output-frame')[0].contentWindow;
            iframeDocument = iframeWindow.document;
            iframeDocument.write("<!DOCTYPE html>");
            iframeDocument.write("<html>");
            iframeDocument.write("<head>");
            // Inject AUI CSS
            if(SANDBOX.env === "flatpack") {
                iframeDocument.write('<link rel="stylesheet" type="text/css" href="../aui/css/aui-all.css">');
            }
            else if(SANDBOX.env === "flatpack-connect") {
                iframeDocument.write('<link rel="stylesheet" type="text/css" href="../auicle/css/aui.css">');
                iframeDocument.write('<link rel="stylesheet" type="text/css" href="../auicle/css/aui-experimental.css">');
            } else {
                iframeDocument.write('<link rel="stylesheet" type="text/css" href="static/aui/css/aui-all.css">');
            }
            iframeDocument.write('<link rel="stylesheet" type="text/css" href="component-output.css">');
            iframeDocument.write("</head>");
            iframeDocument.write("<body></body>");
            iframeDocument.write("</html>");

            // Can't use jQuery here as it will try to load the script with XmlHttpRequest
            var ajsScripts = [];
            if(SANDBOX.env === "flatpack") {
                ajsScripts.push('../aui/js/aui-all.js');
            } else if(SANDBOX.env === "flatpack-connect") {
                ajsScripts.push("http://code.jquery.com/jquery-1.8.3.min.js");
                ajsScripts.push("../auicle/js/aui.js");
                ajsScripts.push("../auicle/js/aui-datepicker.js");
                ajsScripts.push("../auicle/js/aui-experimental.js");
                ajsScripts.push("../auicle/js/aui-soy.js");
            } else {
                ajsScripts.push('static/aui/js/aui-all.js');
            }
            for (var i = 0; i < ajsScripts.length; ++i) {
                var AJSscript = iframeDocument.createElement('script');
                AJSscript.type = 'text/javascript';
                AJSscript.async = false;
                AJSscript.src = ajsScripts[i];
                AJS.$("head", iframeDocument)[0].appendChild(AJSscript);
            }

            //add custom javascript to run
            AJSscript.onload = function(){
                var componentScript = iframeDocument.createElement('script');
                componentScript.src = 'component-output.js';
                AJS.$("head", iframeDocument)[0].appendChild(componentScript);
            };
        }

        $scope.reset = function() {
            // Containers to dump code into
            $head = AJS.$("head", iframeDocument);
            $body = AJS.$("body", iframeDocument);
            $css = AJS.$("<style>").appendTo($head);
            $js = null; // JS is special. Can't just dump into the same <script> tag and expect it to run.
        };


        $scope.output = function() {
            var html = $scope.editors.html.getValue();
            var css = $scope.editors.css.getValue();
            //change background colour of preview depending on what component it is
            var pageLevelComponents = ["pageHeader", "appheader", "horizontalNav"],
                isPageLevel = _.find(pageLevelComponents, function(component){
                    return component == $scope.currentComponent;
                });

            if(isPageLevel){
                $body.addClass("page-level");
            } else {
                $body.removeClass("page-level");
            }

            try {
                $body.html(html);
                $css.html(css);
                iframeWindow.runSandboxJavasript();
            } catch(e) {
                // Ignore
            }
        };

        $scope.outputAndRunJs = function() {
            $scope.reset();
            $scope.output();

            var js = $scope.editors.js.getValue();
            if ($js) $js.remove();
            $js = iframeWindow.AJS.$('<script>').html(js).appendTo($body);
        };

        //run initial setup
        setupIframe();
        $scope.reset();
    }
});